from flask import Flask

from flask_cors import CORS

# Flask stuff
app = Flask(__name__)
CORS(app)


@app.route('/', methods=['GET'])
def users():
    return "Hello world!"
